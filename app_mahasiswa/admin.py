from django.contrib import admin

# Register your models here.
from .models import Status, Pengguna

admin.site.register(Status)
admin.site.register(Pengguna)
