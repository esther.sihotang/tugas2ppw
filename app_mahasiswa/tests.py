from django.test import TestCase
from django.test import Client
from django.urls import resolve
import environ
from .models import *
from .views import *
from .views_profile import *
from .views_status import *
from app_authentication.csui_helper import *

root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False), )
environ.Env.read_env('.env')

class Tugas2AppMahasiswaUnitTest(TestCase):
	def setUp(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")

	def test_tugas2_redirect_url_mahasiswa(self):
		response = Client().get('/mahasiswa/')
		self.assertEqual(response.status_code, 302)

		response = Client().get('/mahasiswa/profile/')
		self.assertEqual(response.status_code, 302)
	
	def test_get_access_token(self):
		username = "Ester Theresia"
		password = "esterrrajaahh"
		coba = get_access_token(username, password)
		self.assertTrue(type(coba), str)
		self.assertIsNone(coba)
		self.assertRaises(Exception, get_access_token(username, password))
		
	def test_profile(self):
		response = self.client.get('/mahasiswa/profile/')
		self.assertEqual(response.status_code, 302)
		# else
		self.client.post('/custom_auth/login/', {'username': self.username, 'password': self.password})
		response = self.client.get('/mahasiswa/profile/')
		self.assertEqual(response.status_code, 200)