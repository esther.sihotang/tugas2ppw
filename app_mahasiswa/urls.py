from django.conf.urls import url
from .views import index
from .views_profile import *
from .views_status import *


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^profile/$', profile, name='profile'),
    url(r'^edit-profile/save/$', save_perubahan, name='save_perubahan'),
    url(r'^add_status/$', add_status, name='add_status'),
    url(r'^delete_status/(?P<id>\w{0,50})/$', delete_status, name='delete_status'),
]
