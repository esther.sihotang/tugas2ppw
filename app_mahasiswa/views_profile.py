from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from .views import response
from django.contrib import messages
from .models import *
from .forms import Status_Form

def profile(request):

    if not 'user_login' in request.session.keys():
        return HttpResponseRedirect('/login/')
    else:
        kode_identitas = request.session['kode_identitas']
        try:
            pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
            set_data_for_session(pengguna)
        except Exception as e:
            pengguna = Pengguna()
            pengguna.kode_identitas = kode_identitas     
            pengguna.username = request.session['user_login']       
            pengguna.save()
            set_data_for_session(pengguna)
        
        html = 'session/homeMhs.html'
        return render(request, html, response)

def set_data_for_session(user):
    response['username'] = user.username
    response['npm'] = user.kode_identitas
    response['foto_profil'] = user.foto_profil
    response['nama'] = user.nama
    pengguna = Pengguna.objects.filter(username=user.username)
    status = Status.objects.filter(pengguna=pengguna).order_by('-id').reverse()
    response['status'] = status
    print(status)
    response['status_form'] = Status_Form

    username = user.username
    user = Pengguna.objects.get(username=username)
    print (user)
    status = Status.objects.filter(pengguna__username=username)
    print (status)
    response['foto_profil'] = user.foto_profil
    response['status'] = status
    response['status_form'] = Status_Form
    response['model'] = status


@csrf_exempt
def save_perubahan(request):
    kode_identitas = request.session['kode_identitas']
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    pengguna.nama = request.POST['nama']
    pengguna.foto_profil = request.POST['foto_profil']
    pengguna.save()
    return HttpResponseRedirect('/mahasiswa/profile/')
    



    



