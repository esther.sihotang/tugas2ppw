from django.db import models

# Create your models here.
class Pengguna(models.Model):
    kode_identitas = models.CharField('npm', max_length=20, primary_key=True, )
    nama = models.CharField('nama', max_length=200, default = 'kosong')
    username = models.CharField('username', max_length=200, default = 'kosong')
    created_at = models.DateTimeField(auto_now_add=True)
    foto_profil = models.CharField('profpic', max_length = 240, default = 'https://imageshack.com/i/pobCn3bBp')

class Status(models.Model):
    status = models.TextField()
    pengguna = models.ForeignKey(Pengguna, null = True)
    created_date = models.DateTimeField(auto_now_add=True)

