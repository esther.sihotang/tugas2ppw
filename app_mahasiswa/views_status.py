from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import Status_Form
from django.contrib import messages
from .models import Status, Pengguna
from.views import response


def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['isi_status'] = request.POST['status']
        username = request.session['user_login']
        user = Pengguna.objects.get(username=username)
        status = Status(pengguna=user,status=response['isi_status'])
        status.save()
        return HttpResponseRedirect('/mahasiswa/profile/')

def delete_status(request, id):
    instance = Status.objects.get(id=id)
    instance.delete()
    return HttpResponseRedirect('/mahasiswa/profile/')
